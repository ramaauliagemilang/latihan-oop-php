<?php

require('animal.php');
require('Frog.php');
require('Ape.php');

$sheep = new Animal("shaun");

echo "Name : $sheep->name <br>"; // "shaun"
echo "legs : $sheep->legs <br>"; // 4
echo "cold blooded : $sheep->cold_blooded <br><br>"; // "no"

$kodok = new Frog("buduk");
echo "Name : $kodok->name <br>"; // "kera sakti"
echo "legs : $kodok->legs <br>"; // 4
echo "cold blooded : $kodok->cold_blooded <br>"; // "no"
$kodok->jump() ; // "hop hop"
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Name : $sungokong->name <br>"; // "kera sakti"
echo "legs : $sungokong->legs <br>"; // 2
echo "cold blooded : $sungokong->cold_blooded <br>"; // "no"
$sungokong->yell(); // "Auooo"